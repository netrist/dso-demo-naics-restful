module.exports.byId = {
  541511: {
    "code": 541511,
    "title": "Custom Computer Programming Services",
    "country": "US",
    "industryDescription": "Professional, Scientific, and Technical Services"
  }
};

module.exports.byIndustry = {
  54151: [{
    "code": 541511,
    "title": "Custom Computer Programming Services",
    "country": "US",
    "industryDescription": "Professional, Scientific, and Technical Services"
  }]
};

module.exports.byGroupId = {
  54: [{
    "code": 541511,
    "title": "Custom Computer Programming Services",
    "country": "US",
    "industryDescription": "Professional, Scientific, and Technical Services"
  }]
};
