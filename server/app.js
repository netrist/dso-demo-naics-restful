const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

module.exports = async () => {
  const app = express();

  // Attach unique request ID for logging
  app.use(require('../util/log').requestId);

  // log http requests
  app.use(morgan('common'));

  // Disable powered by express header
  app.disable('x-powered-by');

  // parse application/json
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

  // Bind routers
  app.use('/rest/naics', require('./routers/naics'));

  // Catch and return formatted errors
  app.use(require('./middleware/handleErrors'));

  // Return generic message for unbound routes
  app.get('/*', (req, res) => res.type('text/plain').send('No routes are configured to handle request for ' + req.url));

  return app;
}
