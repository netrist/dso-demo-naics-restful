const log = require('../../util/log')('routers/naics');
const express = require('express');
const formatResponse = require('../middleware/formatResponse');
const db = require('../db');
const paramMap = require('../../util/params');

const router = express.Router();

router.get('/GetVersion',
  async (req, res, cont) => {
    log.info('Reading version');
    res.body = process.env.GIT_HASH || 'unknown';
    cont();
  },
  formatResponse.sendTextResponse
);

router.post('/GetNAICSById',
  async (req, res, cont) => {
    log.info('Geting NAICS by ID');
    try {
      const params = paramMap({
        naicsId: 'req.body.naicsId'
      }, req, res);

      const result = db.byId[params.naicsId];
      if (result) {
        res.body = {
          "recordCount": 1,
          "results": [result]
        };
      } else {
        res.body = {
          "recordCount": 0,
          "results": []
        }
      }

      log.info('NAICS retrieved by ID');

      cont();
    } catch (err) {
      cont(err);
    }
  },
  formatResponse.sendJSONResponse
);

router.post('/GetNAICSByIndustry',
  async (req, res, cont) => {
    log.info('Geting NAICS by ID');
    try {
      const params = paramMap({
        industryId: 'req.body.industryId'
      }, req, res);

      const results = db.byIndustry[params.industryId];
      if (results) {
        res.body = {
          "recordCount": results.length,
          "results": results
        };
      } else {
        res.body = {
          "recordCount": 0,
          "results": []
        }
      }

      log.info('NAICS retrieved by industry ID');

      cont();
    } catch (err) {
      cont(err);
    }
  },
  formatResponse.sendJSONResponse
);

router.post('/GetNAICSGroupById',
  async (req, res, cont) => {
    log.info('Geting NAICS group by ID');
    try {
      const params = paramMap({
        groupId: 'req.body.groupId'
      }, req, res);

      const results = db.byGroupId[params.groupId];
      if (results) {
        res.body = {
          "recordCount": results.length,
          "results": results
        };
      } else {
        res.body = {
          "recordCount": 0,
          "results": []
        }
      }

      log.info('NAICS group retrieved by ID');

      cont();
    } catch (err) {
      cont(err);
    }
  },
  formatResponse.sendJSONResponse
);

module.exports = router;
