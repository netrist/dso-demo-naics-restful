const formatResponse = exports;

formatResponse.sendEmptyResponse = (req, res) => {
  res.status(200).send();
};

formatResponse.sendTextResponse = (req, res) => {
  res.type('text/plain');
  res.status(200).send(res.body);
};

formatResponse.sendJSONResponse = (req, res) => {
  res.type('json');
  res.status(200).send(res.body);
};

formatResponse.sendSimpleSuccess = (req, res) => {
  res.type('json');
  res.status(200).send({
    success: true
  });
};
