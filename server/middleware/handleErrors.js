const log = require('../../util/log')('util/handleErrors');

module.exports = (err, req, res, cont) => {
  if ((!err.status && !err.statusCode) || err.status === 500 || err.statusCode === 500) {
    if (err.stack) {
      log.error(err.stack);
    } else {
      log.error(err);
    }
  }

  let body;
  if (err.number && err.message) {
    body = {
      "error_code": err.number,
      "error_description": err.message
    }
  } else if (err.status === 401 || err.statusCode === 401) {
    body = {
      "error_code": 4011,
      "error_description": "Application does not have permission"
    };
  } else if (err.status && err.message) {
    body = {
      "error_code": '' + err.status + '0',
      "error_description": err.message || "An unknown error occurred"
    };
  } else {
    body = {
      "error_code": '5000',
      "error_description": "An unknown error occurred"
    };
  }
  res.setHeader('errordescription', body.error_description);
  res.setHeader('errorcode', body.error_code);

  res.status(err.status || err.statusCode || 500).send(body);
}
