const dot = require('dot-object'),
  httpError = require('http-errors'),
  log = require('./log')('params');

const sanitize = (v) => {
  // If param sanitization is required, do it here
  return v;
}

module.exports = (params, req, res) => {
  let optional = false;
  const result = {}, reqRes = {
    req: req,
    res: res
  };
  for (let key in params) {
    if (params.hasOwnProperty(key)) {
      let value = params[key];
      if (key.substring(key.length - 1) === '?') {
        optional = true;
        key = key.substring(0, key.length - 1);
      }

      const p = dot.pick(value, reqRes);
      if (p !== undefined && p !== null) {
        result[key] = sanitize(p);
      } else {
        let expected = value;
        if (Array.isArray(value)) {
          expected = value.join(' or ');
        }
        if (optional) {
          //log.info('Couldn\'t find ' + expected + ' in in ' + req.method + ' on ' + req.originalUrl + ', but ' + (_.isArray(value) && value.length > 1 ? 'they are' : 'it is') + ' marked as optional');
        } else {
          log.error('Couldn\'t find ' + expected + ' in ' + req.method + ' on ' + req.originalUrl);
          throw httpError(400, {message: 'Couldn\'t find ' + expected + ' in ' + req.method + ' on ' + req.originalUrl, code: 4001});
        }
      }
    }
  }

  return result;
};
