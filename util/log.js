const shortid = require('shortid'),
  colors = require('colors'),
  httpContext = require('express-http-context'),
  //path    = require('path'),
  winston = require('winston');

const formatter = function (message) {
  const id = httpContext.get('reqId');
  return (id ? colors.blue('(' + id + ') ') : '') + message;
};

module.exports = (name) => {
  const logger = new (winston.Logger)({
    transports: [

      // Log to console
      new (winston.transports.Console)({
        colorize: true,
        label: name,
        timestamp: true,
        level: 'debug'}),

      // Log to a file
      // new (winston.transports.File)({
      //   label: name,
      //   filename: path.join(__dirname, '../../../app.log'),
      //   maxsize: 10 * 1048576, // once a log file hits 100 mb a new file is created
      //   zippedArchive: true, // zip all but latest
      //   tailable: true, // latest file will always be 'app.log'
      //   maxFiles: 10,
      //   //json: false,
      //   level: 'info'
      // })
    ]
  });

  return {
    log: function (level, message) {
      logger.log(level, formatter(message));
    },
    error: function (message) {
      logger.error(formatter(message));
    },
    warn: function (message) {
      logger.warn(formatter(message));
    },
    verbose: function (message) {
      logger.verbose(formatter(message));
    },
    info: function (message) {
      logger.info(formatter(message));
    },
    debug: function (message) {
      logger.debug(formatter(message));
    },
    silly: function (message) {
      logger.silly(formatter(message));
    }
  }
};

module.exports.requestId = [
  httpContext.middleware,
  (req, res, cont) => {
    const id = shortid.generate();
    httpContext.set('reqId', id);
    res.setHeader('X-Request-Id', id);
    cont();
  }
];
