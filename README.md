# DSO Demo NAICS RESTful Service Pipeline

RESTful NAICS look-up service (for testing). Query for NAICS code 541511, NAICS Group 54541, or NAICS industry 5415.

# About the DevSecOps Pipeline

This project uses a collection of tools to run the code and resulting service artfacts through quality gates:

*Unit Tests* - uses Mocha to fully unit test the NodeJS code

*Code Quality* - uses Sonarcloud.io (SonarQube) to analyze the code and measure it against internal standards for unit test line coverage, security, code styles, etc.

*Application Security* - 
This project uses a collection of tools to run the code and resulting service artfacts through quality gates. The quality gates are excercised **prior to merging code** so that the peer review of the merge request cannot proceed until the quality of the request is considered high.

**Please see the [.gitlab-ci.yml](https://gitlab.com/netrist/dso-demo-naics-restful/-/blob/test/.gitlab-ci.yml) file that enables the quality gates described below!**

### Stages run on every Merge Request: ###

**Unit Tests** - uses [Mocha](https://www.npmjs.com/package/mocha), a popular NodeJS unit testing framework, to fully unit test the NodeJS code

**Code Quality** - uses Sonarcloud.io (SonarQube) [https://sonarcloud.io/](https://sonarcloud.io/) to analyze the code and measure it against internal standards for unit test line coverage, security, code styles, etc

**Application Security** - this phase starts the web service and uses the Zed Attack Proxy [https://www.zaproxy.org/](https://www.zaproxy.org/) to passively scan the web app for top vulnverabilities identified by the Open Web Application Security Project (**OWASP)**

**Container Security** - scans the docker image created to run the web app using [Sysdig Security](https://sysdig.com/products/kubernetes-security/), *prior to pushing the docker image into the image repository* to prevent containers with known vulnerabilities from making it into the organization's image repository.

### Deployment: ###

**Deploy to Kubernetes** - for this example, we proactively deploy the approved image to the Kubernetes cluster using a rolling restart (zero downtime) and tag the image repository to make roll-back simple. 

### Future: ###

Evolving best practice is to use GitOps to update the definition of the deployed platform (Kubernetes specifications) and allow a Kubernetes observer such as ArgoCD to compare the state of the cluster against a good configuration. Look for future demonstrations that implement GitOps!
