FROM node:14.11.0-alpine3.11

ARG GIT_HASH

ENV port 8080
ENV GIT_HASH ${GIT_HASH}

RUN echo "Git hash: "$GIT_HASH

WORKDIR /opt/netrist
ADD server /opt/netrist/server/
ADD util /opt/netrist/util/
ADD main.js /opt/netrist
ADD package.json /opt/netrist
ADD package-lock.json /opt/netrist

RUN ["npm", "install", "--production", "--no-audit"]

EXPOSE 8080
CMD node main.js

