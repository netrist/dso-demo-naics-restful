const log = require('./util/log')('main');

const PORT = 8080 || process.env.port;

async function start () {
  const app = await require('./server/app')();
  await app.listen(PORT);
}

start().then(() => log.info('Server started on port ' + PORT));
