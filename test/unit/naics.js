process.env.NODE_ENV = 'test';

const chai = require('chai');
chai.use(require('chai-http'));
chai.should();
const expect = chai.expect;

describe('naics routes', async () => {
  const app = await require('../../server/app')();

  it('should return a generic response if a GET request cannot be routed', async () => {
    const res = await chai.request(app)
      .get('/fake-route')
      .send();

    res.should.have.status(200);
    res.should.be.text;

    res.text.should.equal('No routes are configured to handle request for /fake-route');
  });

  it('should return "unknown" if not git hash is set', async () => {
    const res = await chai.request(app)
      .get('/rest/naics/GetVersion')
      .send();

    res.should.have.status(200);
    res.should.be.text;

    res.text.should.equal('unknown');
  });

  it('should return a version has set in the env vars', async () => {
    process.env.GIT_HASH = 'abc123';

    const res = await chai.request(app)
      .get('/rest/naics/GetVersion')
      .send();

    res.should.have.status(200);
    res.should.be.text;

    res.text.should.equal('abc123');
  });

  it('should get an NAICS entry by ID', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSById')
      .send({
        "naicsId": 541511
      });

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('recordCount').that.equals(1);
    res.body.should.have.property('results').that.is.an('array');
    res.body.results.should.have.length(1);
    res.body.results[0].should.have.property('code').that.equals(541511);
    res.body.results[0].should.have.property('title').that.equals('Custom Computer Programming Services');
    res.body.results[0].should.have.property('country').that.equals('US');
    res.body.results[0].should.have.property('industryDescription').that.equals('Professional, Scientific, and Technical Services');
  });

  it('should return an empty list when sent an ID with no record', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSById')
      .send({
        "naicsId": 541512
      });

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('recordCount').that.equals(0);
    res.body.should.have.property('results').that.is.an('array');
    res.body.results.should.have.length(0);
  });

  it('should return an error when sent an ID with no ID', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSById')
      .send({
        "naicsId": null
      });

    res.should.have.status(400);
    res.should.be.json;
  });

  it('should get an NAICS entry by industry ID', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSByIndustry')
      .send({
        "industryId": 54151
      });

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('recordCount').that.equals(1);
    res.body.should.have.property('results').that.is.an('array');
    res.body.results.should.have.length(1);
    res.body.results[0].should.have.property('code').that.equals(541511);
    res.body.results[0].should.have.property('title').that.equals('Custom Computer Programming Services');
    res.body.results[0].should.have.property('country').that.equals('US');
    res.body.results[0].should.have.property('industryDescription').that.equals('Professional, Scientific, and Technical Services');
  });

  it('should return an empty list when sent an industry ID with no records', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSByIndustry')
      .send({
        "industryId": 54152
      });

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('recordCount').that.equals(0);
    res.body.should.have.property('results').that.is.an('array');
    res.body.results.should.have.length(0);
  });

  it('should return an error when sent an industry ID with no ID', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSByIndustry')
      .send({
        "industryId": null
      });

    res.should.have.status(400);
    res.should.be.json;
  });

  it('should get an NAICS entry by group ID', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSGroupById')
      .send({
        "groupId": 54
      });

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('recordCount').that.equals(1);
    res.body.should.have.property('results').that.is.an('array');
    res.body.results.should.have.length(1);
    res.body.results[0].should.have.property('code').that.equals(541511);
    res.body.results[0].should.have.property('title').that.equals('Custom Computer Programming Services');
    res.body.results[0].should.have.property('country').that.equals('US');
    res.body.results[0].should.have.property('industryDescription').that.equals('Professional, Scientific, and Technical Services');
  });

  it('should return an empty list when sent an group ID with no records', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSGroupById')
      .send({
        "groupId": 55
      });

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('recordCount').that.equals(0);
    res.body.should.have.property('results').that.is.an('array');
    res.body.results.should.have.length(0);
  });

  it('should return an an error when sent an group ID request with no group ID', async () => {
    const res = await chai.request(app)
      .post('/rest/naics/GetNAICSGroupById')
      .send({
        "groupId": null
      });

    res.should.have.status(400);
    res.should.be.json;
  });

});
